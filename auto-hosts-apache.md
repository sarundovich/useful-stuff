Apache: Dynamic Virtual Hosts
A few months back I looked for a solution to overcome the problem of creating individual Virtual Hosts in Apache every time I wanted to configure a new site on a development machine (something that is a big issue in work where we have a lot of websites). Apache is able to support this functionality relatively easy using a module and a few lines in the configuration file. I set this up on Fedora 14, so results may be slightly different for other OS's (different paths, configuration file setup, etc)

Open up the main Apache conf (/etc/httpd/conf/httpd.conf), and ensure the module mod_vhost_alias is enabled.  There should be a line in the configuration like

LoadModule vhost_alias_module modules/mod_vhost_alias.so
Next, add the following lines to the bottom of this file. You'll need to edit the file with sudo privileges.

NameVirtualHost *:80

UseCanonicalName Off

<VirtualHost *:80>
VirtualDocumentRoot /var/www/html/domains/%0
</VirtualHost>
This sets up a catch all for any domain coming in over port 80 (the default port for http traffic, if your using https you will need to use 443 - alternatively you could remove the port restriction). The important line here is the VirtualDocumentRoot. The tells Apache where your files will reside on disk. The %0 part takes the whole domain name and inserts it into the path. To illustrate this if we went to a domain testing.com.dev the VirtualDocumentRoot would be:
/var/www/html/domains/testing.com.dev
This type of configuration might be suitable for most situations, however I didn't want to have the .dev part of the domain in my folders on disk. I was able to achieve this by setting the VirtualDocumentRoot to:

VirtualDocumentRoot /var/www/html/domains/%-2+
The above example of testing.com.dev would now point to:

/var/www/html/domains/testing.com
Remember to add the domain to your hosts file (/etc/hosts)
